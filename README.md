Dokumentace zdrojových kódů
---------
Body: 2.9/4

1. Vytvořte dokumentaci zdrojových kódů ke 3. projektu.
2. Dokumentaci vytvořte pomocí anotovaných komentářů pro dokumentační nástroj Doxygen (Referenční příručka Doxygen)
3. Dokumentaci odevzdejte jako komentovaný zdrojový soubor proj3.h do informačního systému. Kostra souboru je dostupná zde: proj3.h. Pro překlad dokumentace pomocí nástroje doxygen použijte konfigurační soubor Doxyfile.
4. Použijte minimálně následující klíčová slova pro doxygen:
    * \param a \return
    * \pre a \post
    * \defgroup (případně \addtogroup)

Hodnocení
---------
Strukturálně se bude hodnotit dokumentace následujících částí:
* popisu (datových typů, souboru),
* návratových hodnot,
* parametrů funkcí,
* předpokladů a důsledků funkcí,
* skupin volání/funkcí,
* strukturovaných datových typů a jejich členů,
* dokumentace funkcí.

Významově se držte stručného a výstižného popisu. Jazyk může být anglický, český nebo slovenský (v kódování UTF-8 nebo ASCII).